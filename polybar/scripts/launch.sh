#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have ben shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep; done

# Launch bar
#echo "---" | tee -a /tmp/mainbar.log
#polybar MainBar >> /tmp/mainbar.log 2>&1 &

# Multiple monitors
if type "xrandr"; then
  for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    MONITOR=$m polybar --reload MainBar &
  done
else
  polybar --reload MainBar &
fi
