#!/usr/bin/env bash

echo "Installing required packages..."
pacman -S i3 rofi polybar

if [ $? != 0 ]
    echo "Install: Error "
    exit -1
fi

echo "Success, all packages installed"

sleep 10

echo "Copying config files to right places"

cp i3/config ~/.config/i3/config
echo "i3 config successfull copied"

cp -r rofi/* ~/.config/rofi
echo "Rofi config successfull copied"

cp -r polybar/* ~/.config/polybar
echo "Polybar config successfull copied"
